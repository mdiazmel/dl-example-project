# DL Example Project

Template project for ML/DL pipeline using loggers and artifact uploading

This code is inspired by the template od PL and Hydra proposed [here](https://github.com/ashleve/lightning-hydra-template).

To run it, create a conda environment using this command:

```shell
conda env create -f environment.yml
```
Then activate it:

```shell
conda activate my_dl_project
```

Set environment variables to connect to MLFlow or login to wandb with your personal token using `wandb login`.

For MLFlow:

```shell
# Matchs to s3.accessKeyId in Onyxia
export AWS_ACCESS_KEY_ID=...
# Matchs to s3.secretAccessKey in Onyxia
export AWS_SECRET_ACCESS_KEY=...
export AWS_DEFAULT_REGION='us-east-1'
# Matchs to s3.ssessionToken in Onyxia
export AWS_SESSION_TOKEN=...
# Matchs to s3.endpoint in Onyxia
export MLFLOW_S3_ENDPOINT_URL='https://minio.mpp-inria.fr'
# It corresponds to onyxia.owner with 'user-' prefix in Onyxia
export USER_MLFLOW=user-mdiazmel
# It corresponds to service.password in Onyxia
export PASS_MLFLOW=...
export HYDRA_FULL_ERROR=1
```

For debuging purposes:

```shell
export HYDRA_FULL_ERROR=1
```

Then, run the example:

```
python run.py logger=mlflow
```
or

```shell
python run.py logger=wandb
```
Launch multirunning with hydra:
```shell
python run.py --multirun dataset.batch_size=16,32,64 logger=wandb
```

## Browsing into logger

For MLFlow: https://mlflows.paris.inria.fr/icm/aramisclinica/

For W&B: https://wandb.ai/mdiazmel/projects
